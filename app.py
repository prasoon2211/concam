from flask import Flask, request, redirect, url_for, render_template, session, flash, abort
import os
import json
import glob
from uuid import uuid4
import subprocess
from flask.ext.login import LoginManager, login_user, logout_user, current_user
from flask.ext.sqlalchemy import SQLAlchemy
from redis import Redis
from rq import Queue
from ffvideo import VideoStream

app = Flask(__name__)
app.secret_key = "@#$5534ads324324dw11d1sd1234324"

login_manager = LoginManager()
login_manager.init_app(app)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://test:test@localhost/prasoon'
db = SQLAlchemy(app)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
STATIC_ROOT = os.path.join(APP_ROOT, 'static')

class User(db.Model):
    username = db.Column('username', db.String(80), primary_key=True)
    password =  db.Column('password', db.String(10))
    user_type = db.Column(db.String(15)) # concertgoer, artist

    def __init__(self, username, password, user_type):
        self.username = username
        self.password = password
        self.user_type = user_type

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.username)

    def is_artist(self):
        return True if self.user_type == 'artist' else False

    def __repr__(self):
        return '<User %r>' % self.username

class Audio(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column('name', db.String(80), unique=True)
    location = db.Column('location', db.String(80))
    username = db.Column(db.String(80), db.ForeignKey('user.username'))

    def __init__(self, name, location, username):
        self.name = name
        self.location = location
        self.username = username

@login_manager.user_loader
def load_user(id):
    return User.query.get(str(id))

def get_vids():
    songs = os.listdir(STATIC_ROOT + os.sep + "audio_files")
    thumbs = []
    video_dir = STATIC_ROOT + os.sep + "uploads"
    vids = os.listdir(video_dir)
    all_files = []
    for concert_video_dir in vids:
        for d in os.listdir(video_dir + os.sep + concert_video_dir):
            path_d = video_dir + os.sep + concert_video_dir + os.sep + d
            if os.path.isdir(path_d):
                all_files += [path_d + os.sep + f for f in os.listdir(path_d)]
    all_files = [f for f in all_files if f.find('DS_Store') == -1 and f.find('thumb') == -1]
    all_files.sort(key=lambda x: os.path.getmtime(x))

    all_vids = []
    for i in all_files:
        l = i.split('/')
        watch_url = url_for('show_video', concert=l[-3], uuid=l[-2])
        thumb = url_for('static', filename="uploads/" + l[-3] + os.sep + l[-2] + os.sep + 'thumb.jpg')
        all_vids.append((watch_url, thumb))

    return all_vids

@app.route('/signup' , methods=['GET','POST'])
def signup():
    if request.method == 'GET':
        return render_template('signup.html')

    username = request.form['username']
    password = request.form['password']
    user_type = request.form['user_type']
    if User.query.filter_by(username=username).count():
        flash('User already exists!')
        return render_template('signup.html')

    # create new user
    user = User(request.form['username'], request.form['password'], user_type)
    db.session.add(user)
    db.session.commit()
    flash('User successfully registered')
    return redirect(url_for('login'))

@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == 'GET':
        return render_template('login.html')

    username = request.form['username']
    password = request.form['password']
    registered_user = User.query.filter_by(username=username, password=password).first()
    if registered_user is None:
        flash('Username or Password is invalid' , 'error')
        return redirect(url_for('login'))
    login_user(registered_user)
    flash('Logged in successfully')
    return redirect(request.args.get('next') or url_for('index'))

@app.route('/logout')
def logout():
    logout_user()
    flash('Logged out successfully.')
    return redirect(url_for('index'))

@app.route("/")
def index():
    songs = os.listdir(STATIC_ROOT + os.sep + "audio_files")
    recent_vids = get_vids()
    count = len(recent_vids)
    recent_vids = recent_vids[0:6]
    len_vid = len(recent_vids)
    if len_vid < 6:
        while len_vid < 6:
            recent_vids.append(('#', '#'))
            len_vid += 1

    return render_template("index.html", songs=songs, recent_vids=recent_vids, count=count)

@app.route("/upload", methods=["GET", "POST"])
def upload():
    if request.method == "POST":
        """Handle the upload of a file."""
        form = request.form

        # Create a unique "session ID" for this particular batch of uploads.
        upload_key = str(uuid4())

        # Is the upload using Ajax, or a direct POST by the form?
        is_ajax = False
        if form.get("__ajax", None) == "true":
            is_ajax = True

        # check if audio is present
        concert_name_folder = form.getlist("selectaudio")[0]
        audio_file_location = None
        for f in os.listdir(os.path.join(STATIC_ROOT, 'audio_files', concert_name_folder)):
            if f.index('wav') != -1 or f.index('WAV') != -1:
                audio_file_location = os.path.join(STATIC_ROOT, 'audio_files', concert_name_folder, f)

        # Target folder for videos of these uploads.
        target = os.path.join(STATIC_ROOT, 'uploads', concert_name_folder, upload_key)
        try:
            os.mkdir(target)
        except:
            if is_ajax:
                return ajax_response(False, "Couldn't create upload directory: {}".format(target))
            else:
                return "Couldn't create upload directory: {}".format(target)

        for upload in request.files.getlist("file"):
            filename = upload.filename.rsplit("/")[0]
            destination = "/".join([target, filename])
            session['video_destination'] = destination
            upload.save(destination)
            thumb = VideoStream(destination).get_frame_at_sec(3).image()
            # thumb.thumbnail((265, 165))
            thumb.save(target + os.sep + 'thumb.jpg', "JPEG")


        if audio_file_location:
            session['audio_file_location'] = audio_file_location
            if is_ajax:
                return ajax_response(True, upload_key)
            else:
                return redirect(url_for("upload_complete", uuid=upload_key))
        else:
            flash("Video uploaded but audio not found for this concert. \
                  Your video will be enchanced as soon as audio becomes available.")
            if is_ajax:
                return json.dumps({'status':'upload_no_audio'})
            else:
                return redirect(url_for('index'))

    # Handle the case where user clicks on the upload links
    else:
        return redirect(url_for('index'))


@app.route("/upload_audio", methods=["GET", "POST"])
def upload_audio():

    if request.method == "POST":
        form = request.form

        concert_name = form['selectaudio']
        # Target folder for the audio

        target_audio = STATIC_ROOT + os.sep + "audio_files/{}".format(concert_name)

        target_video = STATIC_ROOT + os.sep + "uploads/{}".format(concert_name)

        for upload in request.files.getlist("file"):
            filename = upload.filename.rsplit("/")[0]
            destination = "/".join([target_audio, filename])
            audio = Audio(concert_name, concert_name + os.sep + filename, current_user.username)
            db.session.add(audio)
            db.session.commit()
            upload.save(destination)
            # Fork off a process and enhance videos in it
            post_enhance_video(destination)

        else:
            flash('File uploaded successfully.')
            return redirect(url_for('index'))

    else:
        songs = os.listdir(STATIC_ROOT + os.sep + "audio_files")
        # Get concerts for which the audio does not exist
        no_audio_songs = []
        for s in songs:
            contents = "".join(os.listdir(STATIC_ROOT + os.sep + "audio_files" + os.sep + s))
            if contents.find('wav') == -1 and contents.find('WAV') == -1:
                no_audio_songs.append(s)
        return render_template("upload_audio.html", songs=no_audio_songs)


@app.route('/watch/', defaults={'page':1})
@app.route('/watch/page/<int:page>')
def watch(page):
    PER_PAGE = 12
    all_vids = get_vids()
    if not all_vids:
        flash("No videos found.")
        return redirect(url_for('index'))

    start_index = PER_PAGE * (page - 1)
    end_index = PER_PAGE * page

    all_vids = all_vids[start_index:end_index]
    if not all_vids:
        abort(404)

    len_vid = len(all_vids)
    if len_vid < 12:
        while len_vid < 12:
            all_vids.append(('#', '#'))
            len_vid += 1

    count = len(all_vids)
    total_pages = count // PER_PAGE + 1
    current_page = page

    return render_template("watch.html", all_vids=all_vids, count=count, total_page=total_pages, current_page=current_page)

@app.route('/about')
def about():
    return render_template("about.html")

def enhance_video(audio_file, video_file):
    """ enhances the video file"""
    enhancer_script = STATIC_ROOT + os.sep + "scripts/enhancer.py"
    subprocess.call(["python", enhancer_script, audio_file, video_file])

def post_enhance_video(audio_file):
    concert_name = audio_file.split('/')[-2]
    concert_video_dir = os.path.join(STATIC_ROOT, 'uploads', concert_name)
    dirs = os.listdir(concert_video_dir)
    all_files = []
    for d in dirs:
        path_d = concert_video_dir + os.sep + d
        if os.path.isdir(path_d):
            all_files += [path_d + os.sep + f for f in os.listdir(path_d)]

    all_files = [f for f in all_files if f.find('DS_Store') == -1 and f.find('thumb') == -1]

    q = Queue(connection=Redis())
    for video_file in all_files:
        enhancer_script = STATIC_ROOT + os.sep + "scripts/enhancer.py"
        q.enqueue(subprocess.call, ["python", enhancer_script, audio_file, video_file])

    if dirs:
        flash("All videos for this concert are being enhanced.")

@app.route("/files/<uuid>")
def upload_complete(uuid):
    """The location we send them to at the end of the upload."""

    # Get their files.
    concert_name_folder = session['video_destination'].split('/')[-3]
    root = os.path.join(STATIC_ROOT, 'uploads', concert_name_folder, uuid)
    if not os.path.isdir(root):
        return "Error: UUID not found!"

    enhance_video(session['audio_file_location'], session['video_destination'])

    return redirect(url_for('show_video',
                            concert=concert_name_folder,
                            uuid=uuid))

@app.route("/watch/<concert>/<uuid>")
def show_video(concert, uuid):
    files = []
    recent_vids = get_vids()[0:3]
    root = os.path.join(STATIC_ROOT, 'uploads', concert, uuid)
    for f in glob.glob("{}/*.*".format(root)):
        fname = f.split("/")[-1]
        files.append(fname) if fname.find('thumb') == -1 else None

    return render_template("file.html",
                           folder=concert + os.sep + uuid,
                           files=files,
                           recent_vids=recent_vids)

def ajax_response(status, msg):
    status_code = "ok" if status else "error"
    return json.dumps(dict(
        status=status_code,
        msg=msg,
    ))

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=8080)
